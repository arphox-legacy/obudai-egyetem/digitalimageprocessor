﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Media.Imaging;

namespace ArphoxDIP
{
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Stopper objektum az eltelt idő mérésére
        /// </summary>
        public static Stopwatch Stopper = new Stopwatch();

        #region Image Loading
        private void KepBetoltes(string path)
        {
            #region Képbetöltés memóriába
            Stopper.Start();
            try
            {
                this.Kep = (Bitmap)Bitmap.FromFile(path);
            }
            catch (Exception e)
            {
                MessageBox.Show(String.Format("Nem tudtam betölteni a fájlt!\n\n({0})", e.Message), "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                Stopper.Stop();
                Stopper.Reset();
                return;
            }
            Stopper.Stop();
            statusTime.Content = String.Format("{0} ms.", Stopper.ElapsedMilliseconds);
            info.LoadTime = Stopper.ElapsedMilliseconds;
            Stopper.Reset();
            #endregion

            #region Típus (RGB / szürke) megállapítása
            //RGB vagy szürkeárnyalatos képet olvastunk be? Tudjuk meg! (PixelFormat nem mérvadó)
            info.LoadedImageType = ImageType.Unknown;
            Bitmap masolat = new Bitmap(Kep);
            Task.Run(() => ImageTypeCalculator(masolat)); //Másolatot kell átadni neki
            #endregion

            #region Takarítás, állítgatás
            //Kép töltés sikeres, ha esetleg hisztogramok lennének betöltve töröljük őket.
            info.LoadedObjectType = ObjectType.Image;
            info.LoadedHistogramType = HistogramType.Nothing;
            hisztogram = null;
            DeleteHistogramContainers();
            info.LoadedImageExtension = Path.GetExtension(path);
            info.LoadedImagePath = path;
            #endregion

            //Kép kirakás, és társai
            ParamLoad_Kep();
        }
        private void HisztogramBetoltes_Gray(string path)
        {
            Stopper.Start();
            try
            {
                this.hisztogram = new GrayscaleHistogram(FileOperations.LoadHistogram(path));
            }
            catch (Exception e)
            {
                MessageBox.Show(String.Format("Nem tudtam betölteni a fájlt!\n\n({0})", e.Message), "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                Stopper.Stop();
                Stopper.Reset();
                return;
            }
            Stopper.Stop();
            statusTime.Content = String.Format("{0} ms.", Stopper.ElapsedMilliseconds);
            info.LoadTime = Stopper.ElapsedMilliseconds;
            Stopper.Reset();

            #region Kirakás az UI-ra
            WindowsFormsHost wfh_hist = new WindowsFormsHost();

            ExternalHistogram.HD hd = new ExternalHistogram.HD();
            wfh_hist.Child = hd;

            midSectionGrid.Children.Add(wfh_hist);

            hd.DrawHistogram(((GrayscaleHistogram)hisztogram).Data);
            #endregion

            //Image elrejtése
            image.Source = null;
            image.Visibility = System.Windows.Visibility.Collapsed;

            info.LoadedObjectType = ObjectType.Histogram;

            Kep = KepMasolat = null;
            info.LoadedImageExtension = null;
            info.LoadedImagePath = null;
            info.LoadedImageType = ImageType.Unknown;

            info.LoadedHistogramType = HistogramType.Grayscale;
            HideFunctions();
        }
        private void HisztogramBetoltes_RGB(string[] paths)
        {
            Stopper.Start();
            RGBHistogram rh = new RGBHistogram();
            try
            {
                rh = new RGBHistogram(
                    FileOperations.LoadHistogram(paths[2]), //B,G,R volt a paths sorrendje, tehát fordítva kell kiolvasni
                    FileOperations.LoadHistogram(paths[1]),
                    FileOperations.LoadHistogram(paths[0])
                );

            }
            catch (Exception e)
            {
                MessageBox.Show(String.Format("Nem tudtam betölteni a fájlt!\n\n({0})", e.Message), "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                Stopper.Stop();
                Stopper.Reset();
                return;
            }
            this.hisztogram = rh;
            Stopper.Stop();
            statusTime.Content = String.Format("{0} ms.", Stopper.ElapsedMilliseconds);
            info.LoadTime = Stopper.ElapsedMilliseconds;
            Stopper.Reset();

            #region Container
            Grid containerGrid = new Grid();

            //RowDefinition-ök felvétele:
            RowDefinition rd1 = new RowDefinition();
            rd1.Height = new GridLength(1, GridUnitType.Star);  //1*
            containerGrid.RowDefinitions.Add(rd1);

            RowDefinition rd2 = new RowDefinition();
            rd2.Height = new GridLength(1, GridUnitType.Star);  //1*
            containerGrid.RowDefinitions.Add(rd2);

            RowDefinition rd3 = new RowDefinition();
            rd3.Height = new GridLength(1, GridUnitType.Star);  //1*
            containerGrid.RowDefinitions.Add(rd3);

            //Hostok felvétele:
            WindowsFormsHost host_red = new WindowsFormsHost();
            ExternalHistogram.HD hd1 = new ExternalHistogram.HD();
            host_red.Child = hd1;
            containerGrid.Children.Add(host_red);

            WindowsFormsHost host_green = new WindowsFormsHost();
            ExternalHistogram.HD hd2 = new ExternalHistogram.HD();
            host_green.Child = hd2;
            containerGrid.Children.Add(host_green);

            WindowsFormsHost host_blue = new WindowsFormsHost();
            ExternalHistogram.HD hd3 = new ExternalHistogram.HD();
            host_blue.Child = hd3;
            containerGrid.Children.Add(host_blue);

            //Sorok beállítása:
            Grid.SetRow(host_red, 0);
            Grid.SetRow(host_green, 1);
            Grid.SetRow(host_blue, 2);

            //Grid hozzáadása
            midSectionGrid.Children.Add(containerGrid);

            //Kirajzolás
            hd1.DrawHistogram(rh.R);
            hd2.DrawHistogram(rh.G);
            hd3.DrawHistogram(rh.B);

            #endregion

            //Image elrejtése
            image.Source = null;
            image.Visibility = System.Windows.Visibility.Collapsed;

            info.LoadedObjectType = ObjectType.Histogram;

            Kep = KepMasolat = null;
            info.LoadedImageExtension = null;
            info.LoadedImagePath = null;
            info.LoadedImageType = ImageType.Unknown;

            info.LoadedHistogramType = HistogramType.RGB;
            HideFunctions();
        }
        private BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp); //lehet hogy ezt át kéne írni
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }


        //Paraméteres betöltések:
        private void ParamLoad_Kep()
        {
            //Kép kirakása
            this.image.Source = BitmapToImageSource(Kep);
            this.image.Width = Kep.Width;
            this.image.Height = Kep.Height;
            image.Visibility = System.Windows.Visibility.Visible;
            ShowFunctions();
        }
        private void ParamLoad_HiszGray()
        {
            #region Kirakás az UI-ra
            WindowsFormsHost wfh_hist = new WindowsFormsHost();

            ExternalHistogram.HD hd = new ExternalHistogram.HD();
            wfh_hist.Child = hd;

            midSectionGrid.Children.Add(wfh_hist);

            hd.DrawHistogram(((GrayscaleHistogram)hisztogram).Data);
            #endregion

            HideFunctions();
        }
        private void ParamLoad_HiszRGB()
        {
            RGBHistogram rh = (RGBHistogram)hisztogram;

            #region Container
            Grid containerGrid = new Grid();

            //RowDefinition-ök felvétele:
            RowDefinition rd1 = new RowDefinition();
            rd1.Height = new GridLength(1, GridUnitType.Star);  //1*
            containerGrid.RowDefinitions.Add(rd1);

            RowDefinition rd2 = new RowDefinition();
            rd2.Height = new GridLength(1, GridUnitType.Star);  //1*
            containerGrid.RowDefinitions.Add(rd2);

            RowDefinition rd3 = new RowDefinition();
            rd3.Height = new GridLength(1, GridUnitType.Star);  //1*
            containerGrid.RowDefinitions.Add(rd3);

            //Hostok felvétele:
            WindowsFormsHost host_red = new WindowsFormsHost();
            ExternalHistogram.HD hd1 = new ExternalHistogram.HD();
            host_red.Child = hd1;
            containerGrid.Children.Add(host_red);

            WindowsFormsHost host_green = new WindowsFormsHost();
            ExternalHistogram.HD hd2 = new ExternalHistogram.HD();
            host_green.Child = hd2;
            containerGrid.Children.Add(host_green);

            WindowsFormsHost host_blue = new WindowsFormsHost();
            ExternalHistogram.HD hd3 = new ExternalHistogram.HD();
            host_blue.Child = hd3;
            containerGrid.Children.Add(host_blue);

            //Sorok beállítása:
            Grid.SetRow(host_red, 0);
            Grid.SetRow(host_green, 1);
            Grid.SetRow(host_blue, 2);

            //Grid hozzáadása
            midSectionGrid.Children.Add(containerGrid);

            //Kirajzolás
            hd1.DrawHistogram(rh.R);
            hd2.DrawHistogram(rh.G);
            hd3.DrawHistogram(rh.B);

            #endregion

            HideFunctions();
        }

        #endregion

        #region UI Specific

        /// <summary>
        /// Berakja az ablakot a képernyő közepére.
        /// </summary>
        private void CenterWindowOnScreen(Window window)
        {
            //http://stackoverflow.com/a/4022379
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            window.Left = (screenWidth / 2) - (windowWidth / 2);
            window.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        /// <summary>
        /// Elrejti azokat a funkciókat, amelyeket csak képekkel lehet végrehajtani.
        /// </summary>
        private void HideFunctions()
        {
            x_pontMenuItem.IsEnabled = false;
            x_histMenuItem.IsEnabled = false;
        }
        /// <summary>
        /// Megjeleníti azokat a funkciókat, amelyeket csak képekkel lehet végrehajtani.
        /// </summary>
        private void ShowFunctions()
        {
            x_pontMenuItem.IsEnabled = true;
            x_histMenuItem.IsEnabled = true;
        }

        /// <summary>
        /// Törli a hisztogramok tárolására szolgáló "container" objektumokat
        /// </summary>
        private void DeleteHistogramContainers()
        {
            for (int i = midSectionGrid.Children.Count - 1; i >= 0; i--)
            {
                if (midSectionGrid.Children[i].GetType() == typeof(WindowsFormsHost) ||
                    midSectionGrid.Children[i].GetType() == typeof(Grid))
                {
                    midSectionGrid.Children.RemoveAt(i);
                }
            }
        }
        #endregion

        #region TaskStart/TaskEnd
        private void TaskStartImage()
        {
            if (Kep != null)    //Az egész képes műveleteknél maradhat null (ott nem kell aktív képnek lennie)
                KepMasolat = new Bitmap(Kep);
            Stopper.Start();
        }
        private void TaskStartHistogram()
        {
            Stopper.Start();
        }
        private void TaskEnd()
        {
            Stopper.Stop();

            //ParamObj feltöltése:
            paramobj_outgoing.LoadTime = Stopper.ElapsedMilliseconds;
            Stopper.Reset();
            if (paramobj_outgoing.Histogram != null)
                paramobj_outgoing.ObjectType = ObjectType.Histogram;
            else
                paramobj_outgoing.ObjectType = ObjectType.Image;

            if (paramobj_outgoing.ObjectType == ObjectType.Image)
            {
                //paramobj_outgoing.Image = már beállította a hívó függvény
                paramobj_outgoing.ImageType = info.LoadedImageType;
            }
            else //info.LoadedObjectType == ObjectType.Histogram
            {
                //paramobj_outgoing.Histogram = már beállította a hívó függvény
                if (paramobj_outgoing.Histogram.GetType() == typeof(RGBHistogram))
                    paramobj_outgoing.HistogramType = HistogramType.RGB;
                else
                    paramobj_outgoing.HistogramType = HistogramType.Grayscale;

            }

            //Új ablak megnyitása:
            MainWindow ujAblak = new MainWindow(paramobj_outgoing);
            paramobj_outgoing = new ParameterObject();
            ujAblak.Show();
        }
        #endregion

        private List<long> TesztStart()
        {
            KepMasolat = new Bitmap(Kep);
            return new List<long>();
        }
        private void TesztEnd(List<long> times)
        {
            string idok = string.Empty;
            for (int i = 0; i < times.Count && i < 20; i++) //Csak az első 20 találatot jelenítjük meg MAX
            {
                idok += (i + 1) + ".\t" + times[i] + " ms.\n";
            }
            if (times.Count > 20)
                idok += "...\n";
            idok += "\nÁtlag: " + times.Average() + " ms.";

            MessageBox.Show(idok, "Eredmény", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        #region Parallel Functions

        unsafe void ImageTypeCalculator(Bitmap KepMasolat)
        {
            //Kép nem lehet null

            System.Drawing.Imaging.BitmapData bmData = KepMasolat.LockBits(new Rectangle(0, 0, KepMasolat.Width, KepMasolat.Height),
                                           System.Drawing.Imaging.ImageLockMode.ReadWrite,
                                           System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            byte* p = (byte*)bmData.Scan0;
            int Height = KepMasolat.Height, Width = KepMasolat.Width;
            int offset = stride - Width * 3;

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (p[0] != p[1] ||
                        p[0] != p[2] ||
                        p[1] != p[2])
                    {
                        info.LoadedImageType = ImageType.RGB;
                        KepMasolat.UnlockBits(bmData);
                        return;
                    }

                    p += 3;
                }
                p += offset;
            }

            KepMasolat.UnlockBits(bmData);

            info.LoadedImageType = ImageType.Grayscale;

            #region Régi, GetPixel-es megoldás
            //for (int y = 0; y < KepMasolat.Height; y++)
            //{
            //    for (int x = 0; x < KepMasolat.Width; x++)
            //    {
            //        Color a = KepMasolat.GetPixel(x, y);

            //        if ((a.R != a.G) || (a.R != a.B) || (a.B != a.G))
            //        {
            //            info.LoadedImageType = ImageType.RGB;
            //            return;
            //        }
            //    }
            //}

            //info.LoadedImageType = ImageType.Grayscale;
            #endregion
        }

        #endregion
    }
}