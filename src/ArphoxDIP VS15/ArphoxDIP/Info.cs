﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ArphoxDIP
{
    public class Info
    {
        public Info(MainWindow mw)
        {
            LinkedWindow = mw;
        }
        public MainWindow LinkedWindow { get; private set; } //azért kell, hogy a Zoom változásakor a statusbart állítani tudjam

        public long LoadTime { get; set; }

        public ObjectType LoadedObjectType { get; set; }

        //Image-specific:
        public string LoadedImagePath { get; set; }
        public string LoadedImageExtension { get; set; }
        public ImageType LoadedImageType { get; set; }

        //Histogram-specifiC:
        public HistogramType LoadedHistogramType { get; set; }

        //Zoom
        private int zoom;
        public int Zoom
        {
            get { return zoom; }
            set
            {
                zoom = value;
                LinkedWindow.statusValues.Visibility = zoom == 0 ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        //Settings
        bool tesztMod = false;
        public bool TesztMod
        {
            get { return tesztMod; }
            set
            {
                tesztMod = value;
                if (tesztMod)
                    LinkedWindow.ChangeBackgroundToTest();
                else
                    LinkedWindow.ChangeBackgroundToNormal();

            }
        }
    }

    public enum ObjectType { Nothing, Image, Histogram };
    public enum ImageType { Unknown, Grayscale, RGB };
    public enum HistogramType { Nothing, Grayscale, RGB };
}