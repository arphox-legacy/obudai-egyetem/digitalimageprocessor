﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ArphoxDIP.Ablakok
{
    public partial class OneDataWindow : Window
    {
        public double Data1 { get; private set; }
        public double Data1StartValue { get; set; }
        public double Data1LowerBound { get; set; }
        public double Data1UpperBound { get; set; }
        public bool Data1_OnlyInteger { get; set; }

        string labelText;

        public OneDataWindow(string labelText)
        {
            InitializeComponent();
            this.labelText = labelText;

            Data1LowerBound = double.MinValue;
            Data1UpperBound = double.MaxValue;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tx1.Text = tx1.Text.Replace('.', ',');
                if (Data1_OnlyInteger)
                    Data1 = int.Parse(tx1.Text);
                else
                    Data1 = double.Parse(tx1.Text);

                if (Data1 > Data1UpperBound || Data1 < Data1LowerBound)
                    throw new ApplicationException("Hibás határok!");

                this.DialogResult = true;
                this.Close();
            }
            catch(Exception x)
            {
                MessageBox.Show(String.Format("Rossz értékeket adtál meg!\n\n{0}", x.Message), "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.Close();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.label.Content = labelText;
            this.tx1.Text = Data1StartValue.ToString();
        }
    }
}
