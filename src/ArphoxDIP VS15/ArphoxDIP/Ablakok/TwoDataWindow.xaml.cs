﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ArphoxDIP.Ablakok
{
    public partial class TwoDataWindow : Window
    {
        public double Data1 { get; private set; }
        public double Data1StartValue { get; set; }
        public double Data1LowerBound { get; set; }
        public double Data1UpperBound { get; set; }
        public bool Data1_OnlyInteger { get; set; }

        public double Data2 { get; private set; }
        public double Data2StartValue { get; set; }
        public double Data2LowerBound { get; set; }
        public double Data2UpperBound { get; set; }
        public bool Data2_OnlyInteger { get; set; }

        string label1Text, label2Text;

        public TwoDataWindow(string label1Text, string label2Text)
        {
            InitializeComponent();
            this.label1Text = label1Text;
            this.label2Text = label2Text;

            Data1LowerBound = double.MinValue;
            Data1UpperBound = double.MaxValue;

            Data2LowerBound = double.MinValue;
            Data2UpperBound = double.MaxValue;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tx1.Text = tx1.Text.Replace('.', ',');
                tx2.Text = tx2.Text.Replace('.', ',');

                if (Data1_OnlyInteger)
                    Data1 = int.Parse(tx1.Text);
                else
                    Data1 = double.Parse(tx1.Text);

                if (Data2_OnlyInteger)
                    Data2 = int.Parse(tx2.Text);
                else
                    Data2 = double.Parse(tx2.Text);

                if (Data1 > Data1UpperBound || Data1 < Data1LowerBound)
                    throw new ApplicationException("Hibás határok az 1. mezőnél!");
                if (Data2 > Data2UpperBound || Data2 < Data2LowerBound)
                    throw new ApplicationException("Hibás határok a 2. mezőnél!");

                this.DialogResult = true;
                this.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(String.Format("Rossz értékeket adtál meg!\n\n{0}", x.Message), "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            label1.Content = label1Text;
            label2.Content = label2Text;

            this.tx1.Text = Data1StartValue.ToString();
            this.tx2.Text = Data2StartValue.ToString();
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.Close();
        }
    }
}