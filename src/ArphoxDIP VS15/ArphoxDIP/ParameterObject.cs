﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArphoxDIP
{
    /// <summary>
    /// Amikor elvégzünk egy műveletet, akkor egy új ablakban nyílik meg az eredmény.
    /// Ezen osztály példánya tartalmazza az átadásra kerülő kép/hisztogram adatait.
    /// </summary>
    public class ParameterObject
    {
        public long LoadTime { get; set; }
        public ObjectType ObjectType { get; set; }

        public Histogram Histogram { get; set; }
        public HistogramType HistogramType { get; set; }

        public Bitmap Image { get; set; }
        public ImageType ImageType { get; set; }
    }
}