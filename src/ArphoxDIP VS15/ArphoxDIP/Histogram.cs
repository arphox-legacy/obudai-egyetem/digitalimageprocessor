﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArphoxDIP
{
    /// <summary>
    /// Ősosztályt biztosít a GrayscaleHistogram és az RGBHistogram osztályoknak
    /// </summary>
    public abstract class Histogram
    {

    }

    public class GrayscaleHistogram : Histogram
    {
        public double[] Data { get; set; }

        public GrayscaleHistogram(double[] data)
        {
            Data = data;
        }
    }
    public class RGBHistogram : Histogram
    {
        public double[] R { get; set; }
        public double[] G { get; set; }
        public double[] B { get; set; }

        public RGBHistogram() //Szintaktikai szerepe van
        {
        }
        public RGBHistogram(double[] R, double[] G, double[] B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }
    }
}